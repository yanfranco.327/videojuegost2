﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Vida : MonoBehaviour
{
    private int VIDAS = 3;
    public Text PuntajeVida;

    public int GetVida()
    {
        return VIDAS; 
    }

    public void QuitarVida (int VIDAS)
    {
        this.VIDAS = VIDAS -VIDAS;
        PuntajeVida.text = "VIDAS: " + GetVida();
    }
}
